package alphasoup;

import alphasoup.searchers.DiagonalLeftSearcher;
import alphasoup.searchers.DiagonalRightSearcher;
import alphasoup.searchers.HorizonSearcher;
import alphasoup.searchers.VerticalSearcher;
import lombok.NonNull;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Main search class
 */
public class WordSearch {
    private DataPreProcessor dataPreProcessor;

    /**
     * Constructor
     * @param input the raw input as list of string
     */
    public WordSearch(@NonNull List<String> input){
        this.dataPreProcessor = new DataPreProcessor(input);
    }


    /**
     * main search function
     * @return a map of search word -> word location
     *         The sequence of the word should match the order in the input file
     */
    public Map<String, WordLocation> runSearch(){
        var result = new LinkedHashMap<String, WordLocation>();
        List<String> wordsToSearch = dataPreProcessor.getWordsToSearch();
        if(wordsToSearch.isEmpty()) {
            return result;
        }

        // can use a factory class to create objects
        HorizonSearcher horizonSearcher = new HorizonSearcher(dataPreProcessor.getMatrixSize(), dataPreProcessor.getWordMatrix());
        VerticalSearcher verticalSearcher = new VerticalSearcher(dataPreProcessor.getMatrixSize(), dataPreProcessor.getWordMatrix());
        DiagonalRightSearcher diagonalRightSearcher = new DiagonalRightSearcher(dataPreProcessor.getMatrixSize(), dataPreProcessor.getWordMatrix());
        DiagonalLeftSearcher diagonalLeftSearcher = new DiagonalLeftSearcher(dataPreProcessor.getMatrixSize(), dataPreProcessor.getWordMatrix());


        for(String wordToSearch: wordsToSearch){
            result.put(
                    wordToSearch,
                    Optional.ofNullable(horizonSearcher.searchStringInList(wordToSearch))
                        .or(()->Optional.ofNullable(verticalSearcher.searchStringInList(wordToSearch)))
                        .or(()->Optional.ofNullable(diagonalRightSearcher.searchStringInList(wordToSearch)))
                        .or(()->Optional.ofNullable(diagonalLeftSearcher.searchStringInList(wordToSearch)))
                        .get());

        }

        return result;
    }

}
