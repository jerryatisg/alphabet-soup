package alphasoup.searchers;

import alphasoup.WordLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for search word diagonal right direction
 */
public class DiagonalRightSearcher extends  SearcherBase{
    public DiagonalRightSearcher(int matrixSize, char[][] wordMatrix){
        super(matrixSize, wordMatrix);
    }

    @Override
    protected WordLocation getLocation(int start, String wordToSearch, int sequence, boolean isReverse) {
        int x = sequence < matrixSize?
                0 :  sequence - matrixSize + 1 ;
        return isReverse?
                new WordLocation(x + start + wordToSearch.length()-1, sequence - (x + start + wordToSearch.length()) + 1,start + x, sequence - (start+ x)):
                new WordLocation(start + x, sequence - (start+ x),x + start + wordToSearch.length()-1, sequence - (x + start + wordToSearch.length()) + 1);

    }

    @Override
    protected List<String> getWordsList() {
        List<String> data = new ArrayList<>();
        for(int i = 0; i<2 * matrixSize -1; i ++ ){
            int arrSize = ( i < matrixSize? i + 1 : (matrixSize-1) * 2 - i + 1 ) ;
            StringBuilder sb = new StringBuilder();
            for(int j =0; j< arrSize; j++){
                int x = 0, y=0;
                if(i < this.matrixSize){
                    x = j;
                    y = i-j;
                }else{
                    x = i - matrixSize + j +1;
                    y = matrixSize -j -1;
                }
                sb.append(wordMatrix[x][y]);
            }
            data.add(sb.toString());
        }
        return data;
    }
}
