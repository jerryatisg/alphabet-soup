package alphasoup.searchers;

import java.util.List;
import alphasoup.WordLocation;

/**
 * Base class for different searchers
 */
public abstract class SearcherBase {
    protected int matrixSize;
    protected char[][] wordMatrix;
    protected SearcherBase(int matrixSize, char[][] wordMatrix){
        this.matrixSize = matrixSize;
        this.wordMatrix = wordMatrix;
    }

    /**
     * Function to get the word location object for the word that is found
     * @param start start index of the word found in the string
     * @param wordToSearch word been searched
     * @param sequence sequence of the string in the list
     * @param isReverse if the search is done reversely
     * @return word location object
     */
    protected abstract  WordLocation getLocation(int start, String wordToSearch,int sequence, boolean isReverse);

    /**
     * Function to create a list of strings from the char matrix based on the search direction
     * @return list of strings
     */
    protected abstract List<String> getWordsList();

    /**
     *
     * @param input  a string that need to be searched
     * @param wordToSearch  the word to search for
     * @param sequence sequence the string that is being searched
     * @return word location if found, otherwise null
     */
    protected WordLocation searchString(String input, String wordToSearch, int sequence){
        int start = input.indexOf(wordToSearch);
        int startRev = input.indexOf(new StringBuilder(wordToSearch).reverse().toString());
        if(start >=0 || startRev >=0){
            return getLocation(startRev  >=0? startRev : start, wordToSearch, sequence,startRev >=0 );
        }
        return null;
    }

    /***
     * Search a word in the list of strings
     * @param wordToSearch is the word to search
     * @return word location if found, otherwise null
     */
    public WordLocation searchStringInList(String wordToSearch){
        List<String> input = getWordsList();
        for( int i=0; i<input.size(); i++){
            WordLocation loc = searchString(input.get(i), wordToSearch, i);
            if(loc != null){
                return loc;
            }
        }
        return null;
    }

}
