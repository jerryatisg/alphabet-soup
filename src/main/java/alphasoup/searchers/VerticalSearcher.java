package alphasoup.searchers;

import alphasoup.WordLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for search word vertically
 */
public class VerticalSearcher extends  SearcherBase{
    public VerticalSearcher(int matrixSize, char[][] wordMatrix){
        super(matrixSize, wordMatrix);
    }

    @Override
    protected WordLocation getLocation(int start, String wordToSearch, int sequence, boolean isReverse) {
        return isReverse?
                new WordLocation(start + wordToSearch.length()-1, sequence, start, sequence) :
                new WordLocation(start, sequence,start + wordToSearch.length()-1, sequence);
    }

    @Override
    protected List<String> getWordsList() {
        List<String> data = new ArrayList<>();
        for(int i = 0; i< matrixSize; i++ ){
            StringBuilder sb = new StringBuilder();
            for(int j = 0; j<this.matrixSize; j++){
                sb.append(wordMatrix[j][i]);
            }
            data.add(sb.toString());
        }
        return data;
    }
}
