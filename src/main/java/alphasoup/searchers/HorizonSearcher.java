package alphasoup.searchers;

import alphasoup.WordLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for search word horizontally
 */
public class HorizonSearcher extends  SearcherBase{
    public HorizonSearcher(int matrixSize, char[][] wordMatrix){
        super(matrixSize, wordMatrix);
    }

    @Override
    protected WordLocation getLocation(int start, String wordToSearch, int sequence, boolean isReverse) {
        return isReverse ?
                new WordLocation(sequence, start + wordToSearch.length()-1, sequence, start) :
                new WordLocation(sequence, start, sequence, start + wordToSearch.length()-1);
    }

    @Override
    protected List<String> getWordsList() {
        ArrayList<String> data = new ArrayList<>();
        for(int i = 0; i< this.matrixSize; i++ ){
            data.add(new String(wordMatrix[i]));
        }
        return data;
    }
}
