package alphasoup.searchers;

import alphasoup.WordLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for search word diagonal left direction
 */
public class DiagonalLeftSearcher extends  SearcherBase{

    public DiagonalLeftSearcher(int matrixSize, char[][] wordMatrix){
        super(matrixSize, wordMatrix);
    }

    @Override
    protected WordLocation getLocation(int start, String wordToSearch, int sequence, boolean isReverse) {
        int xStart = matrixSize - sequence - 1;
        return isReverse?
                new WordLocation(xStart + start + wordToSearch.length()-1, start + wordToSearch.length()-1,xStart + start, start):
                new WordLocation(xStart + start, start, xStart + start + wordToSearch.length()-1, start + wordToSearch.length()-1);

    }

    @Override
    protected List<String> getWordsList() {
        List<String> data = new ArrayList<>();
        for(int i = 0; i<2 * matrixSize -1; i ++ ){
            int arrSize = ( i < matrixSize? i + 1 : (matrixSize-1) * 2 - i + 1 ) ;
            StringBuilder sb = new StringBuilder();
            for(int j =0; j< arrSize; j++){
                int x =0, y=0;
                if(i < this.matrixSize){
                    x = this.matrixSize -i -1 + j;
                    y = j;
                }else{
                    x = j ;
                    y = i - matrixSize + j + 1;
                }
                sb.append(wordMatrix[x][y]);
            }
            data.add(sb.toString());
        }
        return data;
    }
}
