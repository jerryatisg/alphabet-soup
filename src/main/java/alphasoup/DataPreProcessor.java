package alphasoup;

import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Data processor class to process the raw input data
 */
@Getter
public class DataPreProcessor {
    @NonNull
    private List<String> input;

    /**
     * the size of the char matrix
     */
    private int matrixSize;

    /**
     * word matrix in a char array of array
     */
    private  char[][] wordMatrix;

    /**
     * words to search as list of string
     */
    private List<String> wordsToSearch;

    /**
     * @param input raw input data as list of strings
     */
    public DataPreProcessor(List<String> input){
        this.input = input;
        processData();
    }

    private void processData(){
        String dims = input.get(0).trim();
        this.matrixSize = Integer.parseInt(dims.substring(dims.indexOf('x')+1));
        List<String> words = input.subList(1, matrixSize+1);
        this.wordsToSearch = new ArrayList<>();

        this.wordMatrix = new char[matrixSize][matrixSize];
        for(int i =0; i<words.size(); i++){
            wordMatrix[i]=words.get(i).trim().replaceAll("\\s","").toCharArray();
        }

        for(String w : input.subList(matrixSize + 1,input.size())){
            wordsToSearch.add(w.replaceAll("\\s",""));
        }
    }

}
