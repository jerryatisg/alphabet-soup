package alphasoup;

/**
 * Record represent start and end location of a word
 * @param startx Start x location
 * @param starty Start y location
 * @param endx end x location
 * @param endy end y location
 */
public record WordLocation(int startx, int starty, int endx, int endy) {

    public String toString(){
        return String.format("%d:%d %d:%d", startx, starty, endx, endy);
    }
}

