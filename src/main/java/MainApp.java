import alphasoup.WordLocation;
import alphasoup.WordSearch;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class MainApp {

    public static void main(String[] args){
        //can switch to read file path from command line in case it is needed
        String file = "test2.txt";
        try {
            System.out.println("Parsing data from file :" + file);

            List<String> lines = Files.readAllLines(Paths.get(file), StandardCharsets.UTF_8);

            WordSearch search = new WordSearch(lines);

            Map<String, WordLocation> results = search.runSearch();

            results.keySet().forEach( k -> System.out.println( k + " " + results.get(k)));

        }catch (IOException ex){
            System.out.println("Encounter IO error: " + ex.getMessage());
        }
    }
}
