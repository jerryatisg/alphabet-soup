package alphasoup;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class WordSearchTest {
    private WordSearch search;


    @Test
    void runSearch_givenInput_shouldReturnCorrectResult() {
        List<String> input = TestUtil.getFileContent("test1.txt");
        search = new WordSearch(input);
        Map<String, WordLocation> results = search.runSearch();
        assertNotNull(results);
        assertEquals(8, results.size());
        assertEquals("0:0 0:2", results.get("ABC").toString());
        assertEquals("0:0 2:2", results.get("AEI").toString());
        assertEquals("0:2 0:0", results.get("CBA").toString());
        assertEquals("2:2 0:0", results.get("IEA").toString());
        assertEquals("2:1 1:1", results.get("HE").toString());
        assertEquals("0:2 2:2", results.get("CFI").toString());
        assertEquals("2:0 0:2", results.get("GEC").toString());
        assertEquals("0:1 1:0", results.get("BD").toString());
    }

    @Test
    void runsearch_givenInput2_shouldReturnCorrectResult(){
        List<String> input = TestUtil.getFileContent("test2.txt");
        search = new WordSearch(input);
        Map<String, WordLocation> results = search.runSearch();
        assertNotNull(results);
        assertEquals(3, results.size());
        assertEquals("0:0 4:4", results.get("HELLO").toString());
        assertEquals("4:0 4:3", results.get("GOOD").toString());
        assertEquals("1:3 1:1", results.get("BYE").toString());
    }

}