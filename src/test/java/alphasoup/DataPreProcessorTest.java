package alphasoup;

import alphasoup.DataPreProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DataPreProcessorTest {
    private DataPreProcessor processor;

    @BeforeEach
    public void setup(){
        List<String> input = TestUtil.getFileContent("test1.txt");
        processor = new DataPreProcessor(input);
    }

    @Test
    void processData_givenCorrectFile_shouldSetVariablesCorrectly(){
        assertNotNull(processor.getWordMatrix());
        assertEquals(3, processor.getMatrixSize());
        assertEquals(3, processor.getWordMatrix().length);
        assertEquals(8, processor.getWordsToSearch().size());
        assertTrue(processor.getWordsToSearch().contains("ABC"));
        assertTrue(processor.getWordsToSearch().contains("AEI"));
        assertEquals('A', processor.getWordMatrix()[0][0]);
        assertEquals('I', processor.getWordMatrix()[2][2]);
    }

}