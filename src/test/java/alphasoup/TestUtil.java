package alphasoup;



import org.junit.jupiter.api.Disabled;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@Disabled
public class TestUtil {

    public static List<String> getFileContent(String fileName){
        InputStream inputStream = TestUtil.class.getResourceAsStream("/"+fileName);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        return reader.lines().collect(Collectors.toList());
    }

}
