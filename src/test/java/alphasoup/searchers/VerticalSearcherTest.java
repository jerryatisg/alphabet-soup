package alphasoup.searchers;

import alphasoup.DataPreProcessor;
import alphasoup.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class VerticalSearcherTest {
    private DataPreProcessor processor;

    @BeforeEach
    public void setup(){
        List<String> input = TestUtil.getFileContent("test1.txt");
        processor = new DataPreProcessor(input);
    }

    @Test
    void getWordsList_givenCorrectFile_shouldReturncorrectList() {
        VerticalSearcher verticalSearcher = new VerticalSearcher(processor.getMatrixSize(), processor.getWordMatrix());
        List<String> verticals = verticalSearcher.getWordsList();
        assertEquals(3, verticals.size());
        assertEquals("ADG", verticals.get(0));
        assertEquals("CFI", verticals.get(2));
    }
}