package alphasoup.searchers;

import alphasoup.DataPreProcessor;
import alphasoup.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DiagonalRightSearcherTest {
    private DataPreProcessor processor;

    @BeforeEach
    public void setup(){
        List<String> input = TestUtil.getFileContent("test1.txt");
        processor = new DataPreProcessor(input);
    }

    @Test
    void getWordsList_givenCorrectFile_shouldReturncorrectList() {
        DiagonalRightSearcher rightSearcher = new DiagonalRightSearcher(processor.getMatrixSize(), processor.getWordMatrix());
        List<String> diagonalRight= rightSearcher.getWordsList();
        assertEquals(5, diagonalRight.size());
        assertEquals("A", diagonalRight.get(0));
        assertEquals("BD", diagonalRight.get(1));
        assertEquals("CEG", diagonalRight.get(2));
        assertEquals("FH", diagonalRight.get(3));
        assertEquals("I", diagonalRight.get(4));
    }
}