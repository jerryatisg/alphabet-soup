package alphasoup.searchers;

import alphasoup.DataPreProcessor;
import alphasoup.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DiagonalLeftSearcherTest {
    private DataPreProcessor processor;

    @BeforeEach
    public void setup(){
        List<String> input = TestUtil.getFileContent("test1.txt");
        processor = new DataPreProcessor(input);
    }

    @Test
    void getWordsList_givenCorrectFile_shouldReturncorrectList() {
        DiagonalLeftSearcher diagonalLeftSearcher = new DiagonalLeftSearcher(processor.getMatrixSize(), processor.getWordMatrix());
        List<String> diagonalLeft = diagonalLeftSearcher.getWordsList();
        assertEquals(5, diagonalLeft.size());
        assertEquals("G", diagonalLeft.get(0));
        assertEquals("DH", diagonalLeft.get(1));
        assertEquals("AEI", diagonalLeft.get(2));
        assertEquals("BF", diagonalLeft.get(3));
        assertEquals("C", diagonalLeft.get(4));
    }
}