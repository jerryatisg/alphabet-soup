package alphasoup.searchers;

import alphasoup.DataPreProcessor;
import alphasoup.TestUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HorizonSearcherTest {
    private DataPreProcessor processor;

    @BeforeEach
    public void setup(){
        List<String> input = TestUtil.getFileContent("test1.txt");
        processor = new DataPreProcessor(input);
    }

    @Test
    void getWordsList_givenCorrectFile_shouldReturncorrectList() {
        HorizonSearcher horizonSearcher = new HorizonSearcher(processor.getMatrixSize(), processor.getWordMatrix());
        List<String> horizons = horizonSearcher.getWordsList();
        assertEquals(3, horizons.size());
        assertEquals("ABC", horizons.get(0));
        assertEquals("GHI", horizons.get(2));
    }
}